'use strict';

function api(api_method, body) {
	const options = {
		method: 'GET',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + token,
		}
	};
	if (body !== undefined) {
		options.method = 'POST';
		options.body = JSON.stringify(body);
	}
	return fetch('/api/' + api_method, options)
		.then(function(resp) {
			if (!resp.ok) {
				throw 'Method ' + api_method + ' failed with status code ' + resp.status;
			}
			return resp.json();
		})
		.then(function(json) {
			if (json.ok === false) {
				throw json;
			}
			return json;
		});
}

const $ = (id) => document.getElementById(id);

window.addEventListener('DOMContentLoaded', (event) => {
	const $debug = $('debug');

	const log = (msg) => {
		const $p = document.createElement('p');
		$p.innerText = msg;
		$debug.appendChild($p);
		$p.scrollIntoView({
			'behavior': 'auto',
			'block': 'end'
		});
	};

	setInterval(() => {
		api('ping')
			.catch(() => window.location.reload());
	}, 2000);

	const connect = () => {
		api('connect', {})
			.then(() => subscribe())
			.catch(() => {
				log('Connecting...');
				setTimeout(connect, 2000);
			});
	};

	const subscribe = () => {
		const u = new URL(location);
		u.protocol = u.protocol.replace(/^http/, 'ws');
		u.pathname = '/api/subscribe';
		u.search = 'token=' + encodeURIComponent(token);
		try {
			const ws = new WebSocket(u.toString());
			ws.addEventListener('message', (event) => {
				log(event.data);
			});
		} catch (e) {
			log('Subscribe error: ' + e);
			setTimeout(subscribe, 2000);
		}
	};

	$('start').addEventListener('submit', (event) => {
		event.preventDefault();
		event.stopPropagation();

		api('start', {})
			.then(() => connect())
			.catch((e) => log(JSON.stringify(e)));
	});
});

// vim: ai:ts=8:sw=8:noet:syntax=js
