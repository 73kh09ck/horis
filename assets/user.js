'use strict';

const $ = (id) => document.getElementById(id);

window.addEventListener('DOMContentLoaded', (event) => {
	const $debug = $('debug');
	const $message = $('message');

	const log = (msg) => {
		const $p = document.createElement('p');
		$p.innerText = msg;
		$debug.appendChild($p);
		$p.scrollIntoView({
			'behavior': 'auto',
			'block': 'end'
		});
	};

	const $code = $('code');
	const $input = $message.querySelector('input[type=text]');
	const script = $code.innerText.split('\n');
	$code.innerHTML = '';
	script.forEach((line) => {
		const m = /^(\s*)([^ ]+)(: func\(\)\s*{)$/.exec(line);
		if (m) {
			const $node_prefix = document.createTextNode(m[1]);
			const $a = document.createElement('a');
			$a.innerText = m[2];
			$a.setAttribute('href', '#');
			$a.addEventListener('click', (event) => {
				$input.value = '!' + m[2];
				$message.dispatchEvent(new Event('submit'));
			});
			const $node_suffix = document.createTextNode(m[3]);
			$code.appendChild($node_prefix);
			$code.appendChild($a);
			$code.appendChild($node_suffix);
		} else {
			const $node = document.createTextNode(line);
			$code.appendChild($node);
		}
		$code.appendChild(document.createElement('br'));
	});

	$message.addEventListener('submit', (event) => {
		event.preventDefault();
		event.stopPropagation();

		fetch('/msg', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			body: new URLSearchParams(new FormData($message))
		})
			.then(() => {
				$message.reset();
				$input.focus();
			})
			.catch((e) => log(e))
		;
	});
});

// vim: ai:ts=8:sw=8:noet:syntax=js
