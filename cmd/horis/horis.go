/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"

	"gitlab.eientei.org/73kh09ck/horis"
	"gitlab.eientei.org/73kh09ck/horis/script"
	"gitlab.eientei.org/73kh09ck/horis/web"
	"golang.org/x/sync/errgroup"
)

var (
	flInsecure = flag.Bool("insecure", false, "Disable token verification")
)

func init() {
	if len(os.Args) > 0 {
		dir, _ := filepath.Split(os.Args[0])
		dir = strings.TrimSuffix(dir, "/cmd/horis/")
		if dir != "" {
			err := os.Chdir(dir)
			if err != nil {
				log.Fatalf("cannot cd into %q: %s", dir, err)
			}
		}
	}

	flag.Parse()
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	h := &horis.Horis{}
	err := h.Init(ctx)
	if err != nil {
		log.Fatal(err)
	}

	scr := script.New(h)
	err = scr.Init(ctx)
	if err != nil {
		log.Fatalf("cannot init scripting engine: %s", err)
	}

	web := web.New(h, scr)
	err = web.Init(ctx)
	if err != nil {
		log.Fatalf("cannot init web interface: %s", err)
	}
	if *flInsecure {
		web.Insecure = true
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(h.Run(ctx))
	g.Go(web.Run(ctx))
	g.Go(scr.Run(ctx))

	go func() {
		ch := make(chan os.Signal, 1)
		signal.Notify(ch, os.Interrupt)
		log.Println("Horis is ready. Press Ctrl-C to stop.")
		<-ch
		cancel()
	}()

	err = g.Wait()
	if err != nil {
		log.Fatal(err)
	}
}

// vim: ai:ts=8:sw=8:noet:syntax=go
