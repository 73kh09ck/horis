//go:build windows

/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package horis

import (
	"log"
	"os"
	"syscall"
	"unsafe"

	"golang.org/x/sys/windows"
)

var ERROR_NO_ERROR syscall.Errno = 0

func _W(s string) *uint16 {
	wstr, err := syscall.UTF16PtrFromString(s)
	if err != nil {
		log.Panicf("cannot _W(%q): %s", s, err)
	}

	return wstr
}

func __W(s string) *byte {
	return (*byte)(unsafe.Pointer(_W(s)))
}

func (h *Horis) implStart() error {
	kernel32, err := syscall.LoadLibrary("kernel32.dll")
	if err != nil {
		return fmt.Errorf("LoadLibrary(kernel32) failed: %w", err)
	}
	defer windows.FreeLibrary(kernel32)

	var VirtualAllocEx, CreateRemoteThread, LoadLibraryW uintptr

	VirtualAllocEx, err = syscall.GetProcAddress(kernel32, "VirtualAllocEx")
	if err != nil {
		return fmt.Errorf("cannot find VirtualAllocEx in kernel32: %w", err)
	}

	CreateRemoteThread, err = syscall.GetProcAddress(kernel32, "CreateRemoteThread")
	if err != nil {
		return fmt.Errorf("cannot find CreateRemoteThread in kernel32: %w", err)
	}

	LoadLibraryW, err = syscall.GetProcAddress(kernel32, "CreateRemoteThread")
	if err != nil {
		return fmt.Errorf("cannot find LoadLibraryW in kernel32: %w", err)
	}

	var si windows.StartupInfo
	var pi windows.ProcessInformation

	args := append(
		[]string{
			h.Config.ZProg,
			"-file",
			h.pk3Path,
		},
		h.Config.ZArgs...,
	)

	for i, arg := range args {
		args[i] = syscall.EscapeArg(arg)
	}

	err = windows.CreateProcess(
		nil,
		_W(strings.Join(args, " ")),
		nil, nil, false,
		windows.CREATE_SUSPENDED|windows.CREATE_NEW_CONSOLE,
		nil,
		nil,
		&si,
		&pi,
	)
	if err != nil {
		return fmt.Errorf("CreateProcess failed: %w", err)
	}

	ok := false
	defer func() {
		if !ok {
			windows.TerminateProcess(pi.Process, 1)
		}
	}()

	page_rw, _, err := syscall.SyscallN(
		VirtualAllocEx,
		uintptr(pi.Process),
		0,
		windows.MAX_PATH,
		windows.MEM_COMMIT|windows.MEM_RESERVE,
		windows.PAGE_READWRITE,
	)
	if err != nil && err != ERROR_NO_ERROR {
		return fmt.Errorf("VirtualAllocEx (rw) failed: %w", err)
	}

	lib := filepath.Clean(filepath.Join(
		"headcrab",
		"zig-out",
		"lib",
		"headcrab64.dll",
	))
	err = windows.WriteProcessMemory(
		pi.Process,
		page_rw,
		__W(lib),
		uintptr(len(lib)*2),
		nil,
	)
	if err != nil && err != ERROR_NO_ERROR {
		return fmt.Errorf("WriteProcessMemory (rw) failed: %w", err)
	}

	var tid uintptr
	threadHandle, _, err := syscall.SyscallN(
		CreateRemoteThread,
		uintptr(pi.Process),
		0, 0,
		LoadLibraryW,
		page_rw,
		0,
		uintptr(unsafe.Pointer(&tid)),
	)
	if err != nil && err != ERROR_NO_ERROR {
		return fmt.Errorf("CreateRemoteThread (rw) failed: %w", err)
	}

	defer windows.CloseHandle(windows.Handle(threadHandle))

	_, err := windows.WaitForSingleObject(
		windows.Handle(threadHandle),
		windows.INFINITE,
	)
	if err != nil {
		return fmt.Errorf("WaitForSingleObject failed: %w", err)
	}

	_, err = windows.ResumeThread(pi.Thread)
	if err != nil {
		return fmt.Errorf("ResumeThread failed: %w", err)
	}

	ok = true
	return nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
