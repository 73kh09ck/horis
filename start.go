/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package horis

import (
	"context"
	"fmt"
)

func (h *Horis) Start(ctx context.Context) error {
	if h.proc != nil {
		return fmt.Errorf(
			"already started, pid %d",
			h.proc.Pid,
		)
	}

	err := h.implStart()
	if err != nil {
		return err
	}

	proc := h.proc

	go func() {
		<-ctx.Done()
		proc.Kill()
	}()

	go func() {
		proc.Wait()
		h.proc = nil
	}()

	return nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
