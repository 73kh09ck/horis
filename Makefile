all: horis

IMPORT_PATH := gitlab.eientei.org/73kh09ck/horis
VERSION := $(shell git describe --tags --always --match "[0-9][0-9][0-9][0-9].*.*")
DATE := $(shell date -u '+%Y-%m-%d-%H%M UTC')
VERSION_FLAGS := -X "version.Version=$(VERSION)" -X "version.BuildTime=$(DATE)"

clean:
	rm -f cmd/horis/horis

horis: cmd/horis/horis

cmd/horis/horis: $(wildcard */*.go */*/*.go *.go)
	CGO_ENABLED=0 go build -o $@ -v -tags osusergo,netgo -ldflags='-extldflags=-static $(VERSION_FLAGS)' $(IMPORT_PATH)/cmd/horis

fmt: $(wildcard */*.go */*/*.go *.go)
	go fmt ./...

vet: $(wildcard */*.go */*/*.go *.go)
	CGO_ENABLED=0 go vet ./...

assets/crab.o: crab.acs
	acc $< $@

run: cmd/horis/horis assets/crab.o
	NO_BROWSER=1 ./cmd/horis/horis -insecure

.PHONY: clean fmt vet
