const testing = std.testing;
const builtin = @import("builtin");
const std = @import("std");

const prefixes: []const u8 = &[_]u8{ 0xF0, 0xF2, 0xF3, 0x2E, 0x36, 0x3E, 0x26, 0x64, 0x65, 0x66, 0x67 };
pub const nops: []const []const u8 = &.{
    &.{
        0x90,
    }, // NOP1_OVERRIDE_NOP
    &.{
        0x66,
        0x90,
    }, // NOP2_OVERRIDE_NOP
    &.{
        0x0f,
        0x1f,
        0x00,
    }, // NOP3_OVERRIDE_NOP
    &.{
        0x0f,
        0x1f,
        0x40,
        0x00,
    }, // NOP4_OVERRIDE_NOP
    &.{
        0x0f,
        0x1f,
        0x44,
        0x00,
        0x00,
    }, // NOP5_OVERRIDE_NOP
    &.{
        0x66,
        0x0f,
        0x1f,
        0x44,
        0x00,
        0x00,
    }, // NOP6_OVERRIDE_NOP
    &.{
        0x0f,
        0x1f,
        0x80,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // NOP7_OVERRIDE_NOP
    &.{
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // NOP8_OVERRIDE_NOP
    &.{
        0x66,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // NOP9_OVERRIDE_NOP
    &.{
        0x66,
        0x66,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // NOP10_OVERRIDE_NOP
    &.{
        0x66,
        0x66,
        0x66,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // NOP11_OVERRIDE_NOP
    &.{
        0x8d,
        0x76,
        0x00,
    }, // leal 0(%esi),%esi
    &.{
        0x8d,
        0x74,
        0x26,
        0x00,
    }, // leal 0(%esi,1),%esi
    &.{
        0x90,
        0x8d,
        0x74,
        0x26,
        0x00,
    }, // nop; leal 0(%esi,1),%esi
    &.{
        0x8d,
        0xb6,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // leal 0L(%esi),%esi
    &.{
        0x8d,
        0xb4,
        0x26,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // leal 0L(%esi,1),%esi
    &.{
        0x90,
        0x8d,
        0xb4,
        0x26,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // nop; leal 0L(%esi,1),%esi
    &.{
        0x89,
        0xf6,
        0x8d,
        0xbc,
        0x27,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // movl %esi,%esi; leal 0L(%edi,1),%edi
    &.{
        0x8d,
        0x76,
        0x00,
        0x8d,
        0xbc,
        0x27,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // leal 0(%esi),%esi; leal 0L(%edi,1),%edi
    &.{
        0x8d,
        0x74,
        0x26,
        0x00,
        0x8d,
        0xbc,
        0x27,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // leal 0(%esi,1),%esi; leal 0L(%edi,1),%edi
    &.{
        0x8d,
        0xb6,
        0x00,
        0x00,
        0x00,
        0x00,
        0x8d,
        0xbf,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // leal 0L(%esi),%esi; leal 0L(%edi),%edi
    &.{
        0x8d,
        0xb6,
        0x00,
        0x00,
        0x00,
        0x00,
        0x8d,
        0xbc,
        0x27,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // leal 0L(%esi),%esi; leal 0L(%edi,1),%edi
    &.{
        0x8d,
        0xb4,
        0x26,
        0x00,
        0x00,
        0x00,
        0x00,
        0x8d,
        0xbc,
        0x27,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // leal 0L(%esi,1),%esi; leal 0L(%edi,1),%edi
    &.{
        0x66,
        0x2e,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // nopw %cs:0L(%[re]ax,%[re]ax,1)
    &.{
        0x0f,
        0x1f,
        0x44,
        0x00,
        0x00,
        0x66,
        0x0f,
        0x1f,
        0x44,
        0x00,
        0x00,
    }, // nopl 0(%[re]ax,%[re]ax,1); nopw 0(%[re]ax,%[re]ax,1)
    &.{
        0x66,
        0x0f,
        0x1f,
        0x44,
        0x00,
        0x00,
        0x66,
        0x0f,
        0x1f,
        0x44,
        0x00,
        0x00,
    }, // nopw 0(%[re]ax,%[re]ax,1); nopw 0(%[re]ax,%[re]ax,1)
    &.{
        0x66,
        0x0f,
        0x1f,
        0x44,
        0x00,
        0x00,
        0x0f,
        0x1f,
        0x80,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // nopw 0(%[re]ax,%[re]ax,1); nopl 0L(%[re]ax)
    &.{
        0x0f,
        0x1f,
        0x80,
        0x00,
        0x00,
        0x00,
        0x00,
        0x0f,
        0x1f,
        0x80,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // nopl 0L(%[re]ax); nopl 0L(%[re]ax)
    &.{
        0x0f,
        0x1f,
        0x80,
        0x00,
        0x00,
        0x00,
        0x00,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // nopl 0L(%[re]ax); nopl 0L(%[re]ax,%[re]ax,1)
    &.{
        0x66,
        0x66,
        0x2e,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
    &.{
        0x66,
        0x66,
        0x66,
        0x2e,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
    &.{
        0x66,
        0x66,
        0x66,
        0x66,
        0x2e,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // data16; data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
    &.{
        0x66,
        0x66,
        0x66,
        0x66,
        0x66,
        0x2e,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // data16; data16; data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
    &.{
        0x66,
        0x66,
        0x66,
        0x66,
        0x66,
        0x66,
        0x2e,
        0x0f,
        0x1f,
        0x84,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    }, // data16; data16; data16; data16; data16; nopw %cs:0L(%[re]ax,%[re]ax,1)
};

fn readModrm(code: []const u8, ptr: *usize, skipsib: bool) void {
    ptr.* += 1;
    const modrm = code[ptr.*];

    if (!skipsib or code[ptr.*] >= 0x40) {
        var sib = false;
        if (code[ptr.*] < 0xc0 and code[ptr.*] & 7 == 4 and !skipsib) {
            sib = true;
            ptr.* += 1;
        }

        if (modrm >= 0x40 and modrm < 0x80) { // modrm is disp8
            ptr.* += 1;
        } else if ((modrm < 0x40 and modrm & 7 == 5) or (modrm >= 0x80 and modrm < 0xc0)) { // modrm is disp16 or disp32
            ptr.* += 2;
            if (!skipsib) {
                ptr.* += 2;
            }
        } else if (sib and code[ptr.*] & 7 == 5) { // sib is disp8 or disp32
            ptr.* += 1;
            if (modrm & 64 == 0) {
                ptr.* += 3;
            }
        }
    } else if (skipsib and modrm == 0x26) {
        ptr.* += 2;
    }
}

pub fn dis(code: []const u8) usize {
    var off: usize = 0;
    var rexw = false;
    var prefixaddr = false;
    var prefixop = false;
    var ptr: usize = 0;

    var maxprefix: isize = 14;
    while (maxprefix > 0) {
        if (std.mem.indexOfScalar(u8, prefixes, code[ptr]) == null) {
            if (builtin.cpu.arch != .x86_64 or code[ptr] >> 4 != 4) { // not x86-64 or not rex
                break;
            }
        }

        if (code[ptr] == 0x66) {
            prefixop = true;
        } else if (code[ptr] == 0x67) {
            prefixaddr = true;
        } else if (code[ptr] >> 3 == 9) {
            rexw = true;
        }

        ptr += 1;
        maxprefix -= 1;
    }

    if (code[ptr] == 0x0f) { // extended
        ptr += 1;
        if (code[ptr] == 0x38 or code[ptr] == 0x3a) {
            ptr += 1;
            if (code[ptr - 1] == 0x3a) {
                off += 1;
            }

            readModrm(code, &ptr, prefixaddr);
        } else {
            off += switch (code[ptr]) {
                0x80...0x8f => @intCast(usize, 4), // skip disp32
                0x70...0x73 => 1, // skip imm8
                0xa4, 0xc2, 0xc4...0xc6, 0xac, 0xba => 1, // skip imm8
                else => 0,
            };

            if (switch (code[ptr]) {
                0x00...0x03 => true,
                0x0d, 0xa3, 0xa4, 0xa5, 0xab, 0xac, 0xad, 0xae, 0xaf => true,
                0x10...0x6f => true,
                0x70...0x76, 0x78...0x7f => true,
                0x90...0x9f => true,
                0xb0...0xbf => true,
                0xc0...0xc7 => true,
                0xd0...0xff => true,
                else => false,
            }) {
                readModrm(code, &ptr, prefixaddr);
            }
        }
    } else {
        var rexop: usize = 8;
        if (rexw) {
            rexop = 4;
        } else if (prefixop) {
            rexop = 2;
        }
        if (code[ptr] == 0xf6 and code[ptr + 1] & 0x30 == 0) {
            off += 1;
        } else if (code[ptr] == 0xf7 and code[ptr + 1] & 0x30 == 0) {
            off += rexop;
        }
        off += switch (code[ptr]) {
            0xb0...0xb7 => 1,
            0xe0...0xe7 => 1,
            0x70...0x7f => 1,
            0x6a, 0x6b, 0x80, 0x82, 0x83, 0xa8, 0xc0, 0xc1, 0xc6, 0xcd, 0xd4, 0xd5, 0xeb => 1,
            0x04, 0x0c, 0x14, 0x1c, 0x24, 0x2c, 0x34, 0x3c => 1,
            0xc2, 0xca => 2,
            0xc8 => 3,
            0x05, 0x0d, 0x15, 0x1d, 0x25, 0x2d, 0x35, 0x3d => rexop,
            0xb8...0xbf, 0xa0...0xa4 => rexop,
            0x68, 0x69, 0x81, 0xa9, 0xc7, 0xe8, 0xe9 => rexop,
            0x9a, 0xea => 4,
            else => 0,
        };
        if ((code[ptr] == 0x9a or code[ptr] == 0x0a) and !prefixop) {
            off += 2;
        }

        if (switch (code[ptr]) {
            0x62, 0x63, 0x69, 0x6b, 0xc0, 0xc1, 0xc4...0xc7, 0xf6, 0xf7, 0xfe, 0xff => true,
            0x80...0x8f, 0xd0...0xd8 => true,
            0x00...0x03, 0x08...0x0b, 0x10...0x13, 0x18...0x1b => true,
            0x20...0x23, 0x28...0x2b, 0x30...0x33, 0x38...0x3b => true,
            else => false,
        }) {
            readModrm(code, &ptr, prefixaddr);
        }
    }

    return ptr + off + 1;
}

test "x86_64" {
    if (builtin.cpu.arch == .x86_64) {
        try testing.expectEqual(@as(usize, 1), dis(&.{0x55})); // push rbp
        try testing.expectEqual(@as(usize, 2), dis(&.{ 0x84, 0xc0 })); // test al, al
        try testing.expectEqual(@as(usize, 2), dis(&.{ 0x41, 0x56 })); // push r14
        try testing.expectEqual(@as(usize, 2), dis(&.{ 0x31, 0xc0 })); // xor eax, eax
        try testing.expectEqual(@as(usize, 3), dis(&.{ 0x49, 0x89, 0xfa })); // mov r10, rdi
        try testing.expectEqual(@as(usize, 3), dis(&.{ 0x4d, 0x85, 0xed })); // test r13, r13
        try testing.expectEqual(@as(usize, 4), dis(&.{ 0x49, 0x8b, 0x45, 0 })); // mov rax, [r13]
        try testing.expectEqual(@as(usize, 4), dis(&.{ 0x48, 0x8b, 0x58, 0x10 })); // mov rbx, [rax+0x10]
        try testing.expectEqual(@as(usize, 5), dis(&.{ 0x48, 0x89, 0x74, 0x24, 0x48 })); // mov [rsp+0x48], rsi
        try testing.expectEqual(@as(usize, 5), dis(&.{ 0x0f, 0x29, 0x44, 0x24, 0x70 })); // movaps [rsp+0x70], xmm0
        try testing.expectEqual(@as(usize, 6), dis(&.{ 0x0f, 0x85, 0xbb, 0, 0, 0 })); // jne rip+187
        try testing.expectEqual(@as(usize, 6), dis(&.{ 0x8b, 0x2d, 0xaf, 0x17, 0x05, 0x01 })); // mov ebp, [rip+0x10517af]
        try testing.expectEqual(@as(usize, 7), dis(&.{ 0x48, 0x81, 0xec, 0xf0, 0, 0, 0 })); // sub rsp, 0xf0
        try testing.expectEqual(@as(usize, 7), dis(&.{ 0x80, 0x3d, 0x0e, 0xde, 0xd8, 0, 0 })); // cmp [rip+0xd8de0e], 0
        try testing.expectEqual(@as(usize, 8), dis(&.{ 0x0f, 0x29, 0x8c, 0x24, 0x80, 0, 0, 0 })); // movaps [rsp+0x80], xmm1
        try testing.expectEqual(@as(usize, 8), dis(&.{ 0xf2, 0x0f, 0x2a, 0x05, 0x3e, 0x38, 0x81, 0 })); // cvtsi2sdl xmm0, [rip+0x81383e]
    }
}
