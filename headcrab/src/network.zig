const builtin = @import("builtin");
const std = @import("std");

pub var buf: [4096]u8 = undefined;

pub const Server = struct {
    fd: std.os.socket_t,
    remote: std.net.Address,
    is_ready: bool,
    processor: fn ([]const u8) anyerror!void,

    pub fn init(self: *Server, processor: fn ([]const u8) anyerror!void) !void {
        const fd = try std.os.socket(std.os.AF.INET, std.os.SOCK.DGRAM | std.os.SOCK.NONBLOCK | std.os.SOCK.CLOEXEC, 0);
        const localhost = try std.net.Address.parseIp("127.0.0.1", 10666);

        try std.os.setsockopt(
            fd,
            std.os.SOL.SOCKET,
            std.os.SO.REUSEADDR,
            &std.mem.toBytes(@as(c_int, 1)),
        );
        try std.os.bind(fd, &localhost.any, localhost.getOsSockLen());

        self.fd = fd;
        self.is_ready = false;
        self.remote = try std.net.Address.parseIp("0.0.0.0", 0);
        self.processor = processor;
    }

    pub fn process(self: *Server) !void {
        var sender: std.net.Address = try std.net.Address.parseIp("0.0.0.0", 0);
        const n = std.os.recvfrom(self.fd, buf[0..buf.len], 0, &sender.any, &sender.getOsSockLen()) catch |err| {
            switch (err) {
                error.WouldBlock => {
                    return;
                },
                else => {
                    return err;
                },
            }
        };

        if (n < 2 or buf[0] != 0xFF) {
            return;
        }

        switch (buf[1]) {
            52 => {
                buf[0] = 0xFF;
                buf[1] = 35;
                _ = try std.os.sendto(self.fd, buf[0..2], 0, &sender.any, sender.getOsSockLen());
                self.remote = sender;
                self.is_ready = true;
            },
            54 => {
                try self.processor(buf[2..n]);
            },
            56 => {
                self.is_ready = false;
            },
            else => {},
        }
    }

    pub fn send(self: *Server, n: usize) !void {
        if (self.is_ready) {
            _ = try std.os.sendto(self.fd, buf[0..n], 0, &self.remote.any, self.remote.getOsSockLen());
        }
    }
};

// vim: ai:ts=4:sw=4:et:syntax=zig
