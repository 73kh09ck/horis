const builtin = @import("builtin");
const std = @import("std");
const testing = std.testing;
const huffman = @import("./huffman.zig");
const network = @import("./network.zig");
const ll = @import("./ll.zig");

const Segment = enum {
    text,
    data,
    rodata,
};

fn scan_linux(comptime T: type, comptime U: type, segment: Segment, pred: fn ([]const u8, T) ?U, arg: T) !?T {
    const maps = try std.fs.openFileAbsolute("/proc/self/maps", .{ .read = true });
    defer maps.close();

    var buf: [4096]u8 = undefined;
    mapping: while (try maps.reader().readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var it = std.mem.tokenize(u8, line, " ");
        const range = it.next() orelse continue;
        const perms = it.next() orelse continue;
        _ = it.next() orelse continue;
        const dev = it.next() orelse continue;

        if (std.mem.eql(u8, dev, "00:00")) {
            break;
        }

        for (switch (segment) {
            Segment.text => "--xp",
            Segment.data => "rw-p",
            Segment.rodata => "r--p",
        }) |c, i| {
            if ((c != '-') and (perms[i] != c)) {
                continue :mapping;
            }
        }

        it = std.mem.tokenize(u8, range, "-");
        const map_first = std.fmt.parseUnsigned(usize, it.next() orelse continue, 16) catch continue;
        const map_last = std.fmt.parseUnsigned(usize, it.next() orelse continue, 16) catch continue;
        const map_ptr = @intToPtr([*]const u8, map_first);
        const result = pred(map_ptr[0 .. map_last - map_first], arg);

        if (result) |unwrapped| {
            return unwrapped;
        }
    }

    return null;
}

pub const WINAPI: std.builtin.CallingConvention = if (builtin.cpu.arch == .i386)
    .Stdcall
else
    .C;
const windows = std.os.windows;
const MEMORY_BASIC_INFORMATION = extern struct {
    BaseAddress: windows.PVOID,
    AllocationBase: windows.PVOID,
    AllocationProtect: windows.DWORD,
    PartitionId: windows.WORD,
    RegionSize: windows.SIZE_T,
    State: windows.DWORD,
    Protect: windows.DWORD,
    Type: windows.DWORD,
};
extern "kernel32" fn VirtualQuery(lpAddress: ?windows.LPVOID, lpBuffer: *MEMORY_BASIC_INFORMATION, dwLength: windows.SIZE_T) callconv(WINAPI) windows.SIZE_T;
extern "kernel32" fn WriteProcessMemory(hProcess: windows.HANDLE, lpBaseAddress: windows.LPVOID, lpBuffer: windows.LPCVOID, nSize: windows.SIZE_T, lpNumberOfBytesWritten: ?*windows.SIZE_T) callconv(WINAPI) windows.BOOL;

fn scan_windows(comptime T: type, comptime U: type, segment: Segment, pred: fn ([]u8, T) ?U, arg: T) !?T {
    const MEM_FREE = 0x10000;

    var mbi: MEMORY_BASIC_INFORMATION = undefined;
    var pnext: ?windows.LPVOID = null;
    const current = windows.kernel32.GetModuleHandleW(null);

    while (VirtualQuery(pnext, &mbi, @sizeOf(@TypeOf(mbi))) == @sizeOf(@TypeOf(mbi))) {
        pnext = @intToPtr(*anyopaque, @ptrToInt(mbi.BaseAddress) + mbi.RegionSize);
        if (mbi.AllocationBase != @ptrCast(windows.PVOID, current)) {
            continue;
        }
        if (mbi.State == MEM_FREE) {
            continue;
        }

        if (mbi.Protect != @as(windows.DWORD, switch (segment) {
            Segment.text => windows.PAGE_EXECUTE_READ,
            Segment.data => windows.PAGE_READ_WRITE,
            Segment.rodata => windows.PAGE_READONLY,
        })) {
            continue;
        }
        const result = pred(@ptrCast([*]u8, mbi.BaseAddress)[0..mbi.RegionSize], arg);
        if (result) |unwrapped| {
            return unwrapped;
        }
    }
    return null;
}

const scan = switch (builtin.os.tag) {
    .windows => scan_windows,
    else => scan_linux,
};

export const _sendto = switch (builtin.os.tag) {
    .windows => std.os.windows.ws2_32.sendto,
    else => std.os.system.sendto,
};
export const _recvfrom = switch (builtin.os.tag) {
    .windows => std.os.windows.ws2_32.recvfrom,
    else => std.os.system.recvfrom,
};

fn pred_string(mem: []const u8, s: []const u8) ?[]const u8 {
    if (std.mem.indexOf(u8, mem, s)) |offset| {
        return mem[offset .. offset + s.len];
    }
    return null;
}

fn pred_data_ref(mem: []const u8, uptr: *const anyopaque) ?*const anyopaque {
    switch (builtin.cpu.arch) {
        .i386 => {
            const pattern = packed struct {
                push: u8 = 0x68,
                imm32: *const anyopaque,
                call: u8 = 0xe8,
            }{
                .imm32 = uptr,
            };
            const m = (std.mem.indexOf(u8, mem[0..], @ptrCast([*]const u8, &pattern)[0..@sizeOf(@TypeOf(pattern))]) orelse return null) + @sizeOf(@TypeOf(pattern));
            const offset: i32 = std.mem.readVarInt(i32, mem[m .. m + 4], .Little) + 4;
            return @intToPtr(*anyopaque, @bitCast(usize, @bitCast(i32, @ptrToInt(&mem[m])) + offset));
        },
        .x86_64 => {
            const prologue: []const u8 = &[_]u8{ 0x55, 0x48, 0x89, 0xe5 }; // push rbp; mov rbp, rsp
            const prefix: []const u8 = switch (builtin.os.tag) {
                .windows => &[_]u8{ 0xcc, 0xcc, 0xcc },
                else => &[_]u8{0x0},
            };
            var pattern: []const u8 = switch (builtin.os.tag) {
                .windows => &[_]u8{ 0x48, 0x8d, 0x0d }, // lea rcx, [rip+off32]
                else => &[_]u8{ 0x48, 0x8d, 0x3d }, // lea rdi, [rip+off32]
            };
            var offset: usize = 0;
            while (offset < mem.len - 64) {
                const m = (std.mem.indexOf(u8, mem[offset..], pattern) orelse return null) + offset + pattern.len;
                offset = m + 1;
                const load_offset: i32 = std.mem.readVarInt(i32, mem[m .. m + 4], .Little);
                const load_address: usize = @bitCast(usize, @bitCast(i64, @ptrToInt(&mem[m + 4])) + load_offset);
                if (load_address != @ptrToInt(uptr)) {
                    continue;
                }
                std.debug.print("mem = {*}\n", .{&mem[0]});
                std.debug.print("m = {x}\n", .{m});
                const call = (std.mem.indexOf(u8, mem[m..], &.{0xe8}) orelse continue) + (m + 1);
                std.debug.print("call = {x}\n", .{call});
                const call_offset: i32 = std.mem.readVarInt(i32, mem[call .. call + 4], .Little) + 4;
                std.debug.print("call_offset = {x}\n", .{call_offset});
                const target = @bitCast(usize, @bitCast(isize, call) + call_offset);
                std.debug.print("target = {x}\n", .{target});
                if (std.mem.eql(u8, mem[target - prefix.len .. target], prefix)) {
                    return @as(*const anyopaque, &mem[target]);
                } else if (std.mem.eql(u8, mem[target .. target + prologue.len], prologue)) {
                    return @as(*const anyopaque, &mem[target]);
                }

                inline for (ll.nops) |nop, i| {
                    if (std.mem.eql(u8, mem[target - nop.len .. target], nop)) {
                        std.debug.print("found nop pattern {}\n", .{i});
                        return @as(*const anyopaque, &mem[target]);
                    }
                }
            }
            return null;
        },
        else => @compileError("Unsupported target"),
    }
}

fn pred_data_ref_func(mem: []const u8, uptr: *const anyopaque) ?*const anyopaque {
    switch (builtin.cpu.arch) {
        .i386 => {
            const pattern = packed struct {
                push: u8 = 0x68,
                imm32: *const anyopaque,
            }{
                .imm32 = uptr,
            };
            const prefix: []const u8 = switch (builtin.os.tag) {
                .windows => &[_]u8{ 0xcc, 0xcc, 0xcc }, // GCC from MSYS2
                else => &[_]u8{ 0x0, 0x0 }, // FIXME: find a better linux/i386 inter-function gap heruistic
            };

            const m = std.mem.indexOf(u8, mem[0..], @ptrCast([*]const u8, &pattern)[0..@sizeOf(@TypeOf(pattern))]) orelse return null;
            const func = (std.mem.indexOf(u8, mem[0..m], prefix) orelse return null) + @sizeOf(@TypeOf(prefix));
            return @intToPtr(*anyopaque, @bitCast(usize, @bitCast(i32, @ptrToInt(&mem[func]))));
        },
        .x86_64 => {
            var offset: usize = 0;
            while (offset < mem.len - 64) {
                var m = (std.mem.indexOf(u8, mem[offset..], &.{0x8d}) orelse return null) + offset + 2; // lea
                offset = m;
                if (mem[m - 3] & 0xF8 != 0x48) { // REX.W
                    continue;
                }

                const load_offset: i32 = std.mem.readVarInt(i32, mem[m .. m + 4], .Little);
                const load_address: usize = @bitCast(usize, @bitCast(i64, @ptrToInt(&mem[m + 4])) + load_offset);
                if (load_address != @ptrToInt(uptr)) {
                    continue;
                }

                std.debug.print("mem = {*}\n", .{&mem[0]});
                std.debug.print("m = {x}\n", .{m});

                m = m - (m & 0xf);
                while (m > 32) {
                    if (mem[m] & 0xF0 == 0x50 or (mem[m] & 0xF0 == 0x40 and mem[m + 1] & 0xF0 == 0x50)) {
                        if (builtin.os.tag == .windows) {
                            if (mem[m - 1] == 0xcc) { // int3
                                return @as(*const anyopaque, &mem[m]);
                            }
                        }

                        inline for (ll.nops) |nop, i| {
                            if (std.mem.eql(u8, mem[m - nop.len .. m], nop)) {
                                std.debug.print("found nop pattern {}\n", .{i});
                                return @as(*const anyopaque, &mem[m]);
                            }
                        }
                    }
                    m -= 0x10;
                }
            }
            return null;
        },
        else => @compileError("Unsupported target"),
    }
}

var server: network.Server = undefined;

const state = struct {
    var mutex: std.Thread.Mutex = std.Thread.Mutex{};
    var ready: bool = false;
    var okay: bool = false;
    var running: bool = false;
    var tic: u32 = 0;

    var cdocommand: ?*const anyopaque = null;
    var printf: ?*const anyopaque = null;
    var tryruntics: ?*const anyopaque = null;
};

var cmd0: [4096]u8 = undefined;
fn processor_impl(cmd: []const u8) !void {
    std.mem.copy(u8, cmd0[0 .. cmd0.len - 1], cmd);
    cmd0[cmd.len] = 0;

    if (state.cdocommand) |f| {
        switch (builtin.cpu.arch) {
            .i386 => asm volatile (
                \\push %%ecx
                \\call *%%eax
                \\add $4, %%esp
                :
                : [proc] "{eax}" (f),
                  [arg1] "{ecx}" (&cmd[0]),
                : "edx"
            ),
            .x86_64 => switch (builtin.os.tag) {
                .windows => asm volatile ("call *%%rax"
                    :
                    : [proc] "{rax}" (f),
                      [arg1] "{rcx}" (&cmd0[0]),
                    : "rax", "rcx", "rdx", "r8", "r9", "r10", "r11"
                ),
                else => asm volatile ("call *%%rax"
                    :
                    : [proc] "{rax}" (f),
                      [arg1] "{rdi}" (&cmd0[0]),
                    : "rax", "rcx", "rdx", "rsi", "rdi", "r8", "r9", "r10", "r11"
                ),
            },
            else => @compileError("unsupported target"),
        }
    } else {
        std.debug.print("stub_processor: {s}\n", .{cmd});
    }
}

const MAPCHANGE_FMT =
    "\n\x1d\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e" ++
    "\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1e\x1f\n\n" ++
    "\x1c+" ++ "%s - %s\n\n";
const cstr = [*:0]const u8;
export fn printf_hook(fmt: cstr, s1: cstr, s2: cstr) callconv(.C) bool {
    // FIXME: deadlock if printf called by cdocommand
    state.mutex.lock();
    defer state.mutex.unlock();

    if (!state.running) {
        if (state.okay) {
            server.init(processor_impl) catch |err| {
                std.log.err("Server cannot be started: {s}", .{@errorName(err)});
                if (@errorReturnTrace()) |trace| {
                    std.debug.dumpStackTrace(trace.*);
                }
                return false;
            };

            state.running = true;
        } else {
            std.debug.print("Nothing found, do not start the server.\n", .{});
        }
    }

    network.buf[0] = 0xff;
    network.buf[1] = 37;
    var len: usize = 0;
    var hide = false;

    if (std.mem.eql(u8, std.mem.span(fmt), "%s%s")) {
        var buf1 = std.mem.span(s1);
        var buf2 = std.mem.span(s2);
        std.mem.copy(u8, network.buf[2..], buf1);
        std.mem.copy(u8, network.buf[buf1.len + 2 ..], buf2);
        len = 2 + buf1.len + buf2.len;
    } else if (std.mem.eql(u8, std.mem.span(fmt), MAPCHANGE_FMT)) {
        var lump = std.mem.span(s1);
        var name = std.mem.span(s2);
        network.buf[1] = 38;
        network.buf[2] = 2;
        std.mem.copy(u8, network.buf[3..], lump);
        std.mem.copy(u8, network.buf[3 + lump.len ..], " - ");
        std.mem.copy(u8, network.buf[3 + lump.len + 3 ..], name);
        len = 3 + lump.len + 3 + name.len;
    } else if (std.mem.eql(u8, std.mem.span(fmt), "%s\n")) {
        var msg = std.mem.span(s1);
        std.mem.copy(u8, network.buf[2..], msg);
        len = 2 + msg.len;
        if (std.mem.startsWith(u8, msg, "crab:")) {
            hide = true;
        }
    } else {
        return false;
    }

    server.send(len) catch |err| {
        std.log.err("Server cannot send msg: {s}", .{@errorName(err)});
        hide = false;
    };

    return hide;
}

export fn tic_hook() callconv(.C) bool {
    state.mutex.lock();
    defer state.mutex.unlock();

    if (state.running) {
        server.process() catch |err| {
            std.log.err("Server has caught an error: {s}", .{@errorName(err)});
            if (@errorReturnTrace()) |trace| {
                std.debug.dumpStackTrace(trace.*);
            }
        };

        if (state.tic % 64 == 0) {
            processor_impl("pukename crab") catch |err| {
                std.log.err("Cannot puke: {s}", .{@errorName(err)});
                if (@errorReturnTrace()) |trace| {
                    std.debug.dumpStackTrace(trace.*);
                }
            };
        }
        state.tic += 1;
    }

    return false;
}

fn hook_function(orig_addr: *const anyopaque, hook_addr: *const anyopaque) !void {
    var trampoline: [*]u8 = undefined;
    var jump_code: [16]u8 = undefined;

    if (builtin.os.tag == .linux) {
        trampoline = @ptrCast([*]u8, try std.os.mmap(null, 4096, std.os.PROT.WRITE | std.os.PROT.WRITE | std.os.PROT.EXEC, std.os.MAP.PRIVATE | std.os.MAP.ANONYMOUS, -1, 0));
    } else if (builtin.os.tag == .windows) {
        trampoline = @ptrCast([*]u8, try windows.VirtualAlloc(null, 4096, windows.MEM_COMMIT | windows.MEM_RESERVE, windows.PAGE_EXECUTE_READWRITE));
    }

    std.debug.print("Patching func at {*}\n", .{orig_addr});
    std.debug.print("trampoline = {*}\n", .{trampoline});

    std.mem.set(u8, trampoline[0..4096], 0x90); // nop

    var area: usize = 0;
    var jump_size: usize = 0;

    if (builtin.cpu.arch == .i386) {
        jump_size = 5;
        while (area < jump_size) {
            const instructions: []const u8 = @intToPtr([*]const u8, @ptrToInt(orig_addr) + area)[0..24];
            area += ll.dis(instructions);
        }

        jump_code[0] = 0xe9; // call rel32
        std.mem.writeIntNative(i32, jump_code[1..5], @bitCast(i32, @ptrToInt(&trampoline[0])) - @bitCast(i32, @ptrToInt(orig_addr)) - jump_code.len);
    } else {
        jump_size = 13;
        while (area < jump_size) {
            const instructions: []const u8 = @intToPtr([*]const u8, @ptrToInt(orig_addr) + area)[0..24];
            std.debug.print("area was {}, opcode {x}\n", .{ area, instructions[0] });
            area += ll.dis(instructions);
            std.debug.print("area is {}\n", .{area});
        }

        jump_code[0] = 0x49; // mov r10, imm64
        jump_code[1] = 0xba;
        jump_code[10] = 0x41; // jmp r10
        jump_code[11] = 0xff;
        jump_code[12] = 0xe2;
        std.mem.writeIntNative(usize, jump_code[2..10], @ptrToInt(&trampoline[0]));
    }

    std.debug.print("area size is {d}, jump size is {d}.\n", .{ area, jump_size });

    if (builtin.cpu.arch == .i386) {
        trampoline[0] = 0x60; // pusha
        trampoline[1] = 0xe8; // call
        std.mem.writeIntNative(i32, trampoline[2..6], @bitCast(i32, @ptrToInt(hook_addr)) - @bitCast(i32, @ptrToInt(&trampoline[6])));
        trampoline[6] = 0x85; // test eax, eax
        trampoline[7] = 0xc0; // test eax, eax
        trampoline[8] = 0x61; // popa
        std.mem.copy(u8, trampoline[8 .. 8 + area], @intToPtr([*]const u8, @ptrToInt(orig_addr))[0..area]);
        trampoline[8 + area] = 0x80; // push imm32
        std.mem.writeIntNative(usize, @ptrCast(*[4]u8, trampoline[8 + area .. 12 + area]), @ptrToInt(orig_addr) + area);
        trampoline[12 + area] = 0x75; // jnz skip
        trampoline[13 + area] = 0x01;
        trampoline[14 + area] = 0xC3; // ret
        trampoline[15 + area] = 0xC2; // ret 4
        trampoline[16 + area] = 0x04;
        trampoline[17 + area] = 0x00;
    } else {
        trampoline[0] = 0x50; // push rax
        trampoline[1] = 0x51; // push rcx
        trampoline[2] = 0x52; // push rdx
        trampoline[3] = 0x56; // push rsi
        trampoline[4] = 0x57; // push rdi
        trampoline[5] = 0x41; // push r8
        trampoline[6] = 0x50;
        trampoline[7] = 0x41; // push r9
        trampoline[8] = 0x51;
        trampoline[9] = 0x41; // push r10
        trampoline[10] = 0x52;
        trampoline[11] = 0x41; // push r11
        trampoline[12] = 0x53;
        trampoline[13] = 0x48; // mov rax, imm64
        trampoline[14] = 0xb8;
        std.mem.writeIntNative(usize, trampoline[15..23], @ptrToInt(hook_addr));
        trampoline[23] = 0xff; // call rax
        trampoline[24] = 0xd0;
        trampoline[25] = 0x48; // test rax, rax
        trampoline[26] = 0x85;
        trampoline[27] = 0xc0;
        trampoline[28] = 0x41; // pop r11
        trampoline[29] = 0x5b;
        trampoline[30] = 0x41; // pop r10
        trampoline[31] = 0x5a;
        trampoline[32] = 0x41; // pop r9
        trampoline[33] = 0x59;
        trampoline[34] = 0x41; // pop r8
        trampoline[35] = 0x58;
        trampoline[36] = 0x5f; // pop rdi
        trampoline[37] = 0x5e; // pop rsi
        trampoline[38] = 0x5a; // pop rdx
        trampoline[39] = 0x59; // pop rcx
        trampoline[40] = 0x58; // pop rax
        trampoline[41] = 0x75; // jnz skip
        trampoline[42] = (72 + @truncate(u8, area)) - 43;
        std.mem.copy(u8, trampoline[43 .. 51 + area], @intToPtr([*]const u8, @ptrToInt(orig_addr))[0..area]);
        trampoline[51 + area] = 0x50; // push rax
        trampoline[52 + area] = 0x90; // nop
        trampoline[53 + area] = 0x48; // mov rax, imm64
        trampoline[54 + area] = 0xb8;
        std.mem.writeIntNative(usize, @ptrCast(*[8]u8, trampoline[55 + area .. 63 + area]), @ptrToInt(orig_addr) + area);
        trampoline[63 + area] = 0x50; // push rax
        trampoline[64 + area] = 0x48; // mov rax, [rsp+8]
        trampoline[65 + area] = 0x8b;
        trampoline[66 + area] = 0x44;
        trampoline[67 + area] = 0x24;
        trampoline[68 + area] = 0x08;
        trampoline[69 + area] = 0xc2; // ret 8
        trampoline[70 + area] = 0x08;
        trampoline[71 + area] = 0x00;
        trampoline[72 + area] = 0xc2; // skip: ret
        trampoline[73 + area] = 0x00;
        trampoline[74 + area] = 0x00;
    }

    const nop: u8 = 0x90;
    if (builtin.os.tag == .linux) {
        const memfd = try std.os.open("/proc/self/mem", std.os.O.RDWR | std.os.O.CLOEXEC, 0);
        defer std.os.close(memfd);

        _ = try std.os.pwrite(memfd, jump_code[0..jump_size], @as(u64, @ptrToInt(orig_addr)));
        while (jump_size < area) {
            _ = try std.os.pwrite(memfd, @ptrCast([*]const u8, &nop)[0..1], @as(u64, @ptrToInt(orig_addr) + jump_size));
            jump_size += 1;
        }
    } else if (builtin.os.tag == .windows) {
        const current = windows.kernel32.GetCurrentProcess();
        _ = WriteProcessMemory(current, @intToPtr(*anyopaque, @ptrToInt(orig_addr)), &jump_code[0], jump_size, null);
        while (jump_size < area) {
            _ = WriteProcessMemory(current, @intToPtr(*anyopaque, @ptrToInt(orig_addr) + jump_size), &nop, 1, null);
            jump_size += 1;
        }
    }
}

fn start() !void {
    state.mutex.lock();
    defer state.mutex.unlock();
    if (state.ready) {
        return;
    }

    const changed_to = try scan([]const u8, []const u8, Segment.rodata, pred_string, "%s changed to %s\n");
    const toggle_idmypos = try scan([]const u8, []const u8, Segment.rodata, pred_string, "toggle idmypos");
    const gametic_gt_lowtic = try scan([]const u8, []const u8, Segment.rodata, pred_string, "gametic>lowtic");

    state.okay = true;

    if (changed_to) |addr| {
        std.debug.print("changed_to = {*}\n", .{addr});

        state.printf = try scan(*const anyopaque, *const anyopaque, Segment.text, pred_data_ref, @ptrCast(*const anyopaque, addr));
    } else {
        state.okay = false;
    }
    if (toggle_idmypos) |addr| {
        std.debug.print("toggle_idmypos = {*}\n", .{addr});

        state.cdocommand = try scan(*const anyopaque, *const anyopaque, Segment.text, pred_data_ref, @ptrCast(*const anyopaque, addr));
        std.debug.print("CDoCommand = {*}\n", .{state.cdocommand});
    } else {
        state.okay = false;
    }

    if (gametic_gt_lowtic) |addr| {
        std.debug.print("gametic>lowtic = {*}\n", .{addr});

        state.tryruntics = try scan(*const anyopaque, *const anyopaque, Segment.text, pred_data_ref_func, @ptrCast(*const anyopaque, addr));
        std.debug.print("TryRunTics = {*}\n", .{state.tryruntics});
    } else {
        state.okay = false;
    }

    if (state.printf) |addr| {
        try hook_function(addr, printf_hook);
    }

    if (state.tryruntics) |addr| {
        try hook_function(addr, tic_hook);
    }

    state.ready = true;
}

pub fn init() callconv(.C) void {
    start() catch |err| {
        std.log.err("{s}", .{@errorName(err)});
        if (@errorReturnTrace()) |trace| {
            std.debug.dumpStackTrace(trace.*);
        }
    };
}

export const init_array linksection(".init_array") = [1]fn () callconv(.C) void{init};

// vim: ai:ts=4:sw=4:et:syntax=zig
