const std = @import("std");
const testing = std.testing;
const huffman = @embedFile("huffman.dat");
const rhuffman = @embedFile("rhuffman.dat");

const Code = struct {
    codeword: u16,
    bits: u4,
};

fn encode(c: u8) Code {
    const word: u16 = @intCast(u16, huffman[@intCast(u16, c) * 2]) * 256 + @intCast(u16, huffman[@intCast(u16, c) * 2 + 1]);
    const bits: u4 = 3 + @intCast(u4, huffman[@intCast(u16, c) * 2 + 1] & 7);
    const codeword: u16 = (word >> 3) & ((@intCast(u16, 1) << bits) - 1);
    return Code{
        .codeword = codeword,
        .bits = bits,
    };
}

const DecodeResult = struct {
    c: u8,
    codeword: u16,
    bits: u4,
};

fn decode(codeword: u16) !DecodeResult {
    if (codeword >= 1024) {
        return error.InvalidChar;
    }
    const bits: u8 = rhuffman[codeword * 2];
    const c: u8 = rhuffman[codeword * 2 + 1];
    if (bits == 0xff) {
        return error.InvalidChar;
    }
    return DecodeResult{
        .c = c,
        .codeword = codeword & ((@intCast(u16, 1) << @intCast(u4, bits)) - 1),
        .bits = @intCast(u4, bits),
    };
}

test "huffman" {
    try testing.expect(huffman.len == 512);
    try testing.expect(encode(0).codeword == 0b010);
    try testing.expect(encode(0).bits == 3);
    try testing.expect(encode(255).codeword == 0b110000011);
    try testing.expect(encode(255).bits == 9);

    {
        const decoded = try decode(0b1001010001);
        try testing.expectEqual(@intCast(u8, 254), decoded.c);
        try testing.expectEqual(@intCast(u4, 0), decoded.bits);
        try testing.expectEqual(@intCast(u16, 0), decoded.codeword);
    }

    {
        const decoded = try decode(0b1011100101);
        try testing.expectEqual(@intCast(u4, 1), decoded.bits);
        try testing.expectEqual(@intCast(u16, 1), decoded.codeword);
        try testing.expectEqual(@intCast(u8, 2), decoded.c);
    }
}
