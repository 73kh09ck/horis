const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const target = b.standardTargetOptions(.{});
    const libname = switch (target.getCpuArch()) {
        .i386 => "headcrab32",
        .x86_64 => "headcrab64",
        else => "headcrab",
    };
    const lib = b.addSharedLibrary(libname, "src/main.zig", b.version(0, 0, 1));
    lib.setTarget(target);
    if (target.getOsTag() == .windows) {
        lib.linkLibC();
        lib.linkSystemLibrary("ws2_32");
    } else if (target.getOsTag() == .linux and target.getCpuArch() == .i386) {
        lib.force_pic = true;
    }
    lib.setBuildMode(mode);
    lib.install();

    const main_tests = b.addTest("src/huffman.zig");
    main_tests.setBuildMode(mode);

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
}
