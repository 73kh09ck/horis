/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package horis

import (
	"context"
	"errors"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"gitlab.eientei.org/73kh09ck/horis/db"
	"gitlab.eientei.org/73kh09ck/horis/event"
	"gitlab.eientei.org/73kh09ck/horis/zombie"
)

func (h Horis) ReadDir(dirname string) ([]string, error) {
	locals := make(map[string]bool)
	entries, err := os.ReadDir(filepath.Join(h.horisConfigDir, dirname))
	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		return nil, err
	}

	result := make([]string, 0, len(entries))

	for _, entry := range entries {
		if !entry.Type().IsRegular() {
			continue
		}

		if strings.HasSuffix(entry.Name(), ".swp") || strings.HasPrefix(entry.Name(), ".") || strings.HasSuffix(entry.Name(), "~") {
			continue
		}

		locals[entry.Name()] = true
		result = append(result, filepath.Join(h.horisConfigDir, dirname, entry.Name()))
	}

	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	entries, err = os.ReadDir(dirname)
	if err != nil {
		return nil, err
	}

	for _, entry := range entries {
		if !entry.Type().IsRegular() {
			continue
		}
		if strings.HasSuffix(entry.Name(), ".swp") || strings.HasPrefix(entry.Name(), ".") || strings.HasSuffix(entry.Name(), "~") {
			continue
		}

		if locals[entry.Name()] {
			continue
		}

		result = append(result, filepath.Join(wd, dirname, entry.Name()))
	}

	return result, nil
}

type Horis struct {
	horisConfigDir string
	pk3Path        string

	Config   Config
	proc     *os.Process
	Client   *zombie.Client
	Database *db.Database
	Bus      *event.Bus
}

func (h *Horis) Init(ctx context.Context) error {
	config_dir, err := os.UserConfigDir()
	if err != nil {
		return err
	}

	h.horisConfigDir = filepath.Join(config_dir, "horis")

	err = os.MkdirAll(h.horisConfigDir, 0777)
	if err != nil && !errors.Is(err, fs.ErrExist) {
		return err
	}

	h.Config.h = h
	err = h.Config.Load()
	if err != nil {
		return err
	}

	z, err := zombie.MakePK3()
	if err != nil {
		return err
	}

	h.pk3Path = z.Name()
	h.Database, err = db.Open(ctx, filepath.Join(h.horisConfigDir, "db"))
	if err != nil {
		return err
	}

	h.Bus = event.NewBus()
	h.Client, err = zombie.Dial(
		"udp",
		h.Config.ZAddr,
		&zombie.ClientConfig{},
	)
	if err != nil {
		return err
	}

	return nil
}

func (h *Horis) Run(ctx context.Context) func() error {
	return func() error {
		<-ctx.Done()
		h.Database.Wait()
		os.Remove(h.pk3Path)
		return h.Config.Store()
	}
}

// vim: ai:ts=8:sw=8:noet:syntax=go
