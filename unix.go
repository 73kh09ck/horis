//go:build linux || freebsd || openbsd || netbsd || darwin

/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package horis

import (
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
)

func (h *Horis) implStart() error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}

	args := append(
		[]string{
			"-file",
			h.pk3Path,
		},
		h.Config.ZArgs...,
	)
	cmd := exec.Command(
		h.Config.ZProg,
		args...,
	)
	env_name := "LD_PRELOAD"
	if runtime.GOOS == "darwin" {
		env_name = "DYLD_INSERT_LIBRARIES"
	}
	cmd.Env = append(
		os.Environ(),
		env_name+"="+filepath.Clean(filepath.Join(
			wd,
			"headcrab",
			"zig-out",
			"lib",
			"libheadcrab64.so",
		)),
	)
	cmd.Dir = filepath.Dir(h.Config.ZProg)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Start()
	if err != nil {
		return err
	}

	h.proc = cmd.Process
	return nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
