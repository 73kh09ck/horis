/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package script

import (
	"encoding/gob"

	"gitlab.eientei.org/73kh09ck/horis/db"

	"github.com/ozanh/ugo"
	"github.com/ozanh/ugo/token"
)

func init() {
	gob.Register(ugo.Object(ugo.Int(0)))
	gob.Register(ugo.String(""))
	gob.Register(ugo.Int(0))
	gob.Register(ugo.Uint(0))
	gob.Register(ugo.Map{})
	gob.Register(ugo.SyncMap{})
	gob.Register(ugo.Array{})
	gob.Register(ugo.Bool(false))
	gob.Register(ugo.Bytes{})
	gob.Register(ugo.Char(' '))
	gob.Register(ugo.Error{})
	gob.Register(ugo.Float(0.0))
	gob.Register(ugo.ObjectPtr{})
	gob.Register(ugo.UndefinedType{})
}

type Database db.Database
type Object = ugo.Object

type DBIterator struct {
	d  *Database
	ch <-chan db.KeyValue

	key   string
	value Object
}

func (i *DBIterator) Next() bool {
	kv, ok := <-i.ch
	if !ok {
		return false
	}

	i.key = kv.Key
	var err error
	i.value, err = ugo.ToObject(kv.Value)
	if err != nil {
		i.value = ugo.Undefined
	}
	return true
}

func (i *DBIterator) Key() Object {
	return ugo.String(i.key)
}

func (i *DBIterator) Value() Object {
	return i.value
}

func (*Database) TypeName() string {
	return "database"
}

func (*Database) String() string {
	return "<database>"
}

func (*Database) CanIterate() bool { return true }

func (d *Database) Iterate() ugo.Iterator {
	return &DBIterator{ch: (*db.Database)(d).Iterate()}
}

func (d *Database) BinaryOp(_ token.Token, _ Object) (Object, error) {
	return nil, ugo.ErrInvalidOperator
}

func (d *Database) IndexSet(key, value ugo.Object) error {
	(*db.Database)(d).Set(key.String(), value)
	return nil
}

func (d *Database) IndexGet(key Object) (Object, error) {
	return ugo.ToObject((*db.Database)(d).Get(key.String()))
}

func (d *Database) Equal(_ Object) bool {
	return false
}

func (d *Database) IsFalsy() bool {
	return false
}

func (d *Database) CanCall() bool {
	return false
}

func (d *Database) Call(_ ...Object) (Object, error) {
	return nil, ugo.ErrNotCallable
}

// vim: ai:ts=8:sw=8:noet:syntax=go
