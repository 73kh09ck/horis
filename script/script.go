/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package script

import (
	"context"
	"fmt"
	"log"
	"sync"

	"gitlab.eientei.org/73kh09ck/horis"
	"gitlab.eientei.org/73kh09ck/horis/event"

	"github.com/ozanh/ugo"
	ustrings "github.com/ozanh/ugo/stdlib/strings"
)

type Script struct {
	Horis *horis.Horis

	ctx     context.Context
	c       *ugo.VM
	globals ugo.Map
	st      *ugo.SymbolTable
	locals  []ugo.Object
	mu      sync.Mutex
	stop    chan struct{}
}

func New(h *horis.Horis) *Script {
	return &Script{
		Horis: h,
		stop:  make(chan struct{}),
	}
}

func (s *Script) run(name string, args ...ugo.Object) error {
	sym, ok := s.st.Resolve(name)
	if !ok {
		syms := s.st.Symbols()
		names := make([]string, 0, len(syms))
		for _, sym := range syms {
			if sym.Scope == ugo.ScopeLocal {
				names = append(names, sym.Name)
			}
		}

		return fmt.Errorf("no %s found, got %+v", name, names)
	}
	_, err := s.c.RunCompiledFunction(s.locals[sym.Index].(*ugo.CompiledFunction), s.globals, args...)

	return err
}

func (s *Script) Init(ctx context.Context) error {
	return s.load(s.Horis.Config.Script())
}

func (s *Script) load(body string) (err error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.st = ugo.NewSymbolTable()
	moduleMap := ugo.NewModuleMap()
	moduleMap.AddBuiltinModule("strings", ustrings.Module)
	moduleMap.AddBuiltinModule("util", UtilModule)
	bytecode, err := ugo.Compile([]byte(body), ugo.CompilerOptions{
		OptimizerMaxCycle: 100,
		OptimizeConst:     true,
		OptimizeExpr:      true,
		SymbolTable:       s.st,
		ModuleMap:         moduleMap,
	})
	if err != nil {
		return err
	}
	s.globals = ugo.Map{
		"from":     ugo.String(""),
		"database": (*Database)(s.Horis.Database),
		"time":     TimeModule,
		"doom":     &Crab{Script: s},
	}
	s.c = ugo.NewVM(bytecode)
	_, err = s.c.Run(s.globals)
	if err != nil {
		return
	}

	locals := make([]ugo.Object, 16)
	s.locals = s.c.GetLocals(locals)

	err = s.run("init")

	return
}

func (s *Script) Load(body string) error {
	s.stop <- struct{}{}

	err := s.load(body)
	if err != nil {
		return err
	}

	go s.Run(s.ctx)()

	return nil
}

func (s *Script) Run(ctx context.Context) func() error {
	return func() error {
		s.ctx = ctx
		running := true
		s.Horis.Bus.OnMessage(func(m event.Message) bool {
			s.mu.Lock()
			defer s.mu.Unlock()

			s.globals["from"] = ugo.String(m.From)

			err := s.run("onmessage", ugo.String(m.Body))
			if err != nil {
				log.Printf("script: onmessage failed: %s", err)
			}

			return running
		})
		defer func() {
			running = false
		}()
		for {
			select {
			case <-s.stop:
				return nil

			case task := <-Schedule:
				_, err := s.c.RunCompiledFunction(task.fn.(*ugo.CompiledFunction), s.globals, task.args...)
				if err != nil {
					log.Printf("script: cannot run scheduled task: %s", err)
				}

			case <-ctx.Done():
				return nil
			}
		}
	}
}

// vim: ai:ts=8:sw=8:noet:syntax=go
