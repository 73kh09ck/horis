/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package script

import (
	"bytes"
	"fmt"

	"github.com/ozanh/ugo"
	"github.com/ozanh/ugo/token"
)

type Crab struct {
	Script *Script
}

func (*Crab) TypeName() string {
	return "database"
}

func (*Crab) String() string {
	return "<database>"
}

func (*Crab) CanIterate() bool { return false }

func (c *Crab) Iterate() ugo.Iterator {
	return nil
}

func (c *Crab) BinaryOp(_ token.Token, _ Object) (Object, error) {
	return nil, ugo.ErrInvalidOperator
}

func (c *Crab) IndexSet(key, value ugo.Object) error {
	return ugo.ErrInvalidOperator
}

func (c *Crab) IndexGet(key Object) (Object, error) {
	return nil, ugo.ErrInvalidOperator
}

func (c *Crab) Equal(_ Object) bool {
	return false
}

func (c *Crab) IsFalsy() bool {
	return false
}

func (c *Crab) CanCall() bool {
	return true
}

func (c *Crab) Call(args ...Object) (Object, error) {
	if len(args) < 1 {
		return nil, ugo.ErrWrongNumArguments.NewError(
			fmt.Sprintf("want>=1 got=%d", len(args)),
		)
	}

	buf := new(bytes.Buffer)
	for i, arg := range args {
		if i > 0 {
			buf.WriteRune(' ')
			buf.WriteString(fmt.Sprintf("%q", arg.String()))
		} else {
			buf.WriteString(arg.String())
		}
	}
	c.Script.Horis.Client.Send(buf.String())

	return nil, nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
