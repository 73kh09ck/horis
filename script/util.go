/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package script

import (
	"fmt"
	"log"
	"math/rand"

	"github.com/ozanh/ugo"
)

var UtilModule = ugo.Map{
	"log": &ugo.Function{
		Value: func(args ...ugo.Object) (ugo.Object, error) {
			if len(args) < 1 {
				return nil, ugo.ErrWrongNumArguments.NewError(
					fmt.Sprintf("want>=1 got=%d", len(args)),
				)
			}
			if args[0].TypeName() != "string" {
				return nil, ugo.NewIndexTypeError("string", args[0].TypeName())
			}
			xs := make([]interface{}, len(args)-1)
			for i, arg := range args[1:] {
				xs[i] = arg
			}
			log.Printf("script: "+args[0].String(), xs...)
			return nil, nil
		},
		Name: "log",
	},
	"fmt": &ugo.Function{
		Value: func(args ...ugo.Object) (ugo.Object, error) {
			if len(args) < 1 {
				return nil, ugo.ErrWrongNumArguments.NewError(
					fmt.Sprintf("want>=1 got=%d", len(args)),
				)
			}
			if args[0].TypeName() != "string" {
				return nil, ugo.NewIndexTypeError("string", args[0].TypeName())
			}
			xs := make([]interface{}, len(args)-1)
			for i, arg := range args[1:] {
				xs[i] = arg
			}
			return ugo.String(fmt.Sprintf(args[0].String(), xs...)), nil
		},
		Name: "fmt",
	},
	"roll": &ugo.Function{
		Value: func(args ...ugo.Object) (ugo.Object, error) {
			if len(args) < 1 {
				return nil, ugo.ErrWrongNumArguments.NewError(
					fmt.Sprintf("want>=1 got=%d", len(args)),
				)
			}

			ps := []float64{}
			vs := []ugo.Object{}
			var m, sum, offset float64
			for i := 0; i < len(args)-1; i += 2 {
				flt, ok := ugo.ToGoFloat64(args[i])
				if !ok {
					return ugo.Undefined, ugo.NewArgumentTypeError(fmt.Sprintf("%dth", i), "float64", args[i].TypeName())
				}

				ps = append(ps, flt)
				vs = append(vs, args[i+1])
				if flt > m {
					m = flt
				}
				sum += m
			}
			if len(args)%2 == 1 {
				last := args[len(args)-1]
				ps = append(ps, m)
				vs = append(vs, last)
				sum += m
			}
			roll := rand.Float64()
			log.Printf("Roll value is %f", roll)
			for i, p := range ps {
				value := offset + p/sum
				if roll < value {
					return vs[i], nil
				}
				offset = value
			}
			return ugo.Undefined, nil // should not happen
		},
		Name: "roll",
	},
}

// vim: ai:ts=8:sw=8:noet:syntax=go
