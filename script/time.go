/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package script

import (
	"fmt"
	"time"

	"github.com/ozanh/ugo"
)

type task struct {
	fn   ugo.Object
	args []ugo.Object
}

var Schedule = make(chan task, 100)

var TimeModule = ugo.Map{
	"now": &ugo.Function{
		Name: "now",
		Value: func(args ...ugo.Object) (ugo.Object, error) {
			if len(args) != 0 {
				return nil, ugo.ErrWrongNumArguments.NewError(
					fmt.Sprintf("want=0 got=%d", len(args)),
				)
			}

			return ugo.Int(time.Now().Unix()), nil
		},
	},
	"sleep": &ugo.Function{
		Name: "sleep",
		Value: func(args ...ugo.Object) (ugo.Object, error) {
			if len(args) != 1 {
				return nil, ugo.ErrWrongNumArguments.NewError(
					fmt.Sprintf("want=1 got=%d", len(args)),
				)
			}

			s, ok := ugo.ToGoFloat64(args[0])
			if !ok {
				return ugo.Undefined, ugo.NewArgumentTypeError("1st", "float64", args[0].TypeName())
			}

			if !args[1].CanCall() {
				return ugo.Undefined, ugo.NewArgumentTypeError("2nd", "function", args[1].TypeName())
			}

			d := time.Duration(s * float64(time.Second))
			time.Sleep(d)

			return ugo.Undefined, nil
		},
	},
	"delay": &ugo.Function{
		Name: "delay",
		Value: func(args ...ugo.Object) (ugo.Object, error) {
			if len(args) < 2 {
				return nil, ugo.ErrWrongNumArguments.NewError(
					fmt.Sprintf("want>=2 got=%d", len(args)),
				)
			}

			s, ok := ugo.ToGoFloat64(args[0])
			if !ok {
				return ugo.Undefined, ugo.NewArgumentTypeError("1st", "float64", args[0].TypeName())
			}

			go func() {
				d := time.Duration(s * float64(time.Second))
				time.Sleep(d)

				Schedule <- task{args[1], args[2:]}
			}()

			return ugo.Undefined, nil
		},
	},
}

// vim: ai:ts=8:sw=8:noet:syntax=go
