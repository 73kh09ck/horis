/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db

import (
	"context"
	"encoding/gob"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Database struct {
	dir   string
	lockf *os.File

	mu   *sync.RWMutex
	m    map[string]interface{}
	w    map[string]bool
	v    int64
	done chan struct{}
}

func (db *Database) run(ctx context.Context) {
	t := time.NewTicker(time.Second * 10)
	defer t.Stop()

	running := true
	for running {
		select {
		case <-ctx.Done():
			running = false
		case <-t.C:
			db.Flush()
		}
	}

	db.Flush()
	db.lockf.Truncate(0)
	db.lockf.Close()
	os.Remove(filepath.Join(db.dir, "_lock"))
	close(db.done)
}

func (db *Database) Wait() {
	<-db.done
}

func (db *Database) Set(key string, value interface{}) {
	db.mu.Lock()
	defer db.mu.Unlock()

	if db.w == nil {
		db.w = map[string]bool{key: true}
	} else {
		db.w[key] = true
	}

	db.m[key] = value
}

func (db *Database) Delete(key string) bool {
	db.mu.Lock()
	defer db.mu.Unlock()

	_, ok := db.m[key]
	if !ok {
		return false
	}

	if db.w == nil {
		db.w = map[string]bool{key: true}
	} else {
		db.w[key] = true
	}

	delete(db.m, key)
	return true
}

func (db *Database) Get(key string) interface{} {
	db.mu.RLock()
	defer db.mu.RUnlock()

	return db.m[key]
}

func (db *Database) Contains(key string) bool {
	db.mu.RLock()
	defer db.mu.RUnlock()

	_, ok := db.m[key]
	return ok
}

func (db *Database) Range(f func(key string, value interface{}) interface{}) interface{} {
	db.mu.RLock()
	defer db.mu.RUnlock()

	for k, v := range db.m {
		r := f(k, v)
		if r != nil {
			return r
		}
	}

	return nil
}

func (db *Database) Transact(f func(key string, value interface{}) interface{}) interface{} {
	keys := map[string]bool{}
	db.mu.RLock()
	for k := range db.m {
		keys[k] = true
	}
	db.mu.RUnlock()

	for k := range keys {
		db.mu.RLock()
		v, ok := db.m[k]
		db.mu.RUnlock()

		if !ok {
			continue
		}

		r := f(k, v)
		if r != nil {
			return r
		}
	}

	return nil
}

type KeyValue struct {
	Key   string
	Value interface{}
}

func (db *Database) Iterate() <-chan KeyValue {
	keys := map[string]bool{}
	db.mu.RLock()
	for k := range db.m {
		keys[k] = true
	}
	db.mu.RUnlock()

	ch := make(chan KeyValue, 16)

	go func() {
		for k := range keys {
			db.mu.RLock()
			v, ok := db.m[k]
			db.mu.RUnlock()

			if !ok {
				continue
			}

			ch <- KeyValue{Key: k, Value: v}
		}
		close(ch)
	}()

	return ch
}

func (db *Database) Flush() {
	db.mu.Lock()
	defer db.mu.Unlock()

	if db.w == nil {
		return
	}

	prev := db.v
	db.v += 1

	f, err := os.OpenFile(
		filepath.Join(db.dir, fmt.Sprintf("%d.gob", db.v)),
		os.O_CREATE|os.O_TRUNC|os.O_WRONLY,
		0666,
	)
	if err != nil {
		log.Panicf("db failure: %s", err)
	}
	enc := gob.NewEncoder(f)
	err = enc.Encode(db.m)
	if err != nil {
		log.Panicf("db failure: %s", err)
	}
	err = f.Close()
	if err != nil {
		log.Panicf("db failure: %s", err)
	}

	err = os.WriteFile(
		filepath.Join(db.dir, "CURRENT"),
		[]byte(strconv.FormatInt(db.v, 10)+"\n"),
		0666,
	)
	if err != nil {
		log.Panicf("db failure: %s", err)
	}

	db.w = nil

	os.Remove(filepath.Join(db.dir, fmt.Sprintf("%d.gob", prev)))
}

func (db *Database) load() error {
	db.v = 0
	db.m = make(map[string]interface{})
	db.w = nil

	current, err := os.ReadFile(filepath.Join(db.dir, "CURRENT"))
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return nil
		}
		return err
	}

	db.v, err = strconv.ParseInt(strings.TrimSpace(string(current)), 10, 64)
	if err != nil {
		return err
	}

	f, err := os.Open(filepath.Join(db.dir, fmt.Sprintf("%d.gob", db.v)))
	if err != nil {
		return err
	}
	defer f.Close()

	dec := gob.NewDecoder(f)
	err = dec.Decode(&db.m)

	return err
}

func Open(ctx context.Context, dir string) (*Database, error) {
	fi, err := os.Stat(dir)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			err = os.Mkdir(dir, 0777)
			if err != nil {
				return nil, err
			}
			fi, err = os.Stat(dir)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	if !fi.IsDir() {
		return nil, &fs.PathError{
			Op:   "open",
			Path: dir,
			Err:  fmt.Errorf("not a directory"),
		}
	}

	lockf, err := os.OpenFile(
		filepath.Join(dir, "_lock"),
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC,
		0666,
	)
	if err != nil {
		return nil, err
	}

	_, err = fmt.Fprintf(lockf, "%d\n", os.Getpid())
	if err != nil {
		return nil, err
	}

	err = lockf.Sync()
	if err != nil {
		return nil, err
	}

	db := &Database{
		dir:   dir,
		lockf: lockf,
		mu:    new(sync.RWMutex),
		done:  make(chan struct{}),
	}

	err = db.load()
	if err != nil {
		return nil, err
	}

	go db.run(ctx)
	return db, nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
