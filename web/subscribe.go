/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package web

import (
	"bytes"
	"encoding/json"
	"log"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/websocket"
)

func (w *Web) APISubscribe(c *gin.Context) (interface{}, error) {
	handler := websocket.Handler(func(ws *websocket.Conn) {
		defer ws.Close()
		ch := w.Horis.Client.Subscribe(c.Request.Context())
		buf := new(bytes.Buffer)
		enc := json.NewEncoder(buf)
		for {
			select {
			case msg := <-ch:
				err := enc.Encode(msg)
				if err != nil {
					log.Printf("cannot marshal msg: %s", err)
					return
				}

				buf.WriteRune('\n')
				_, err = ws.Write(buf.Bytes())
				buf.Reset()
				if err != nil {
					log.Printf("cannot send msg: %s", err)
				}
			}
		}
	})
	handler.ServeHTTP(c.Writer, c.Request)
	return nil, nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
