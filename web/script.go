/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package web

import (
	"github.com/gin-gonic/gin"
)

func (w *Web) APIScript(c *gin.Context) (interface{}, error) {
	var m struct {
		Script string `json:"script"`
	}
	if err := c.ShouldBind(&m); err != nil {
		return nil, err
	}

	err := w.script.Load(m.Script)
	if err != nil {
		return nil, err
	}

	return map[string]bool{
		"ok": true,
	}, nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
