/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package web

import (
	"fmt"
	"net/http"
	"reflect"

	"github.com/gin-gonic/gin"
)

func (w *Web) APIMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if w.Insecure {
			return
		}

		if q := c.Query("token"); q != "" {
			if q != w.token {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
					"ok":    false,
					"error": "ErrInvalidToken",
				})
			}

			return
		}
		if c.GetHeader("Authorization") != "Bearer "+w.token {
			c.AbortWithStatusJSON(http.StatusOK, gin.H{
				"ok":    false,
				"error": "ErrInvalidToken",
			})
		}
	}
}

func (w *Web) apiwrap(impl func(c *gin.Context) (interface{}, error)) func(c *gin.Context) {
	return func(c *gin.Context) {
		result, err := impl(c)

		if err != nil {
			c.JSON(http.StatusOK, map[string]interface{}{
				"ok":          false,
				"error":       fmt.Sprintf("%T", err),
				"description": err.Error(),
			})
			return
		}

		if result == nil || (reflect.TypeOf(result).Kind() == reflect.Map && reflect.ValueOf(result).IsNil()) {
			c.JSON(http.StatusOK, map[string]bool{"ok": true})
		} else {
			c.JSON(http.StatusOK, result)
		}
	}
}

func (w *Web) initAPI(g *gin.RouterGroup) error {
	g.Use(w.APIMiddleware())

	g.GET("/ping", w.apiwrap(w.APIPing))
	g.POST("/start", w.apiwrap(w.APIStart))
	g.POST("/connect", w.apiwrap(w.APIConnect))
	g.GET("/subscribe", w.apiwrap(w.APISubscribe))
	g.POST("/fire/onmessage", w.apiwrap(w.APIFireOnMessage))
	g.POST("/script", w.apiwrap(w.APIScript))

	return nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
