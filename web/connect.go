/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package web

import (
	"time"

	"github.com/gin-gonic/gin"
)

func (w *Web) APIConnect(c *gin.Context) (interface{}, error) {
	t := time.NewTicker(time.Second)
	defer t.Stop()

	for {
		if w.Horis.Client.IsReady() {
			return nil, nil
		}

		select {
		case <-c.Request.Context().Done():
			return nil, nil
		case <-t.C:
		}
	}
}

// vim: ai:ts=8:sw=8:noet:syntax=go
