/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package web

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"html/template"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/gin-gonic/contrib/renders/multitemplate"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/render"

	"gitlab.eientei.org/73kh09ck/horis"
	"gitlab.eientei.org/73kh09ck/horis/event"
	"gitlab.eientei.org/73kh09ck/horis/script"
)

type Web struct {
	Horis    *horis.Horis
	Assets   map[string]string
	Insecure bool

	engine   *gin.Engine
	token    string
	listener net.Listener
	ctx      context.Context
	script   *script.Script
}

func New(h *horis.Horis, scr *script.Script) *Web {
	return &Web{
		Horis:  h,
		script: scr,
	}
}

func (w *Web) initRoutes(g *gin.RouterGroup) error {
	g.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"Script": w.Horis.Config.Script(),
		})
	})
	g.POST("/msg", func(c *gin.Context) {
		var m event.Message
		if err := c.Bind(&m); err != nil {
			log.Printf("msg bind error: %s", err)
			return
		}
		m.From = c.RemoteIP()
		w.Horis.Bus.FireOnMessage(m)
	})
	g.GET("/admin", func(c *gin.Context) {
		if !net.ParseIP(c.RemoteIP()).IsLoopback() {
			c.AbortWithStatus(http.StatusForbidden)
			return
		}

		c.HTML(http.StatusOK, "admin.html", gin.H{
			"Token": w.token,
		})
	})
	w.initAPI(g.Group("/api"))

	return nil
}

func (w *Web) initRender(g *gin.RouterGroup) (render.HTMLRender, error) {
	var err error
	var data []byte
	var tmpl *template.Template

	var names, pnames []string

	template_files, err := w.Horis.ReadDir("templates")
	if err != nil {
		return nil, err
	}
	for _, name := range template_files {
		if strings.HasPrefix(filepath.Base(name), "_") {
			pnames = append(pnames, name)
		} else {
			names = append(names, name)
		}
	}

	funcs := template.FuncMap{
		"join": strings.Join,
	}

	render := multitemplate.New()
	ptmpls := make(map[string]*template.Template)
	for _, pname := range pnames {
		if data, err = os.ReadFile(pname); err != nil {
			return nil, fmt.Errorf("cannot open %q from tbox while iterating over pnames: %w", pname, err)
		}
		pname = strings.TrimSuffix(filepath.Base(pname), ".html")
		if tmpl, err = template.New(pname).Funcs(funcs).Parse(string(data)); err != nil {
			return nil, fmt.Errorf("cannot parse template %q: %w", pname, err)
		}
		ptmpls[pname] = tmpl
	}
	for _, name := range names {
		if data, err = os.ReadFile(name); err != nil {
			return nil, fmt.Errorf("cannot open %q from tbox while iterating over names: %w", name, err)
		}
		if tmpl, err = template.New(filepath.Base(name)).Funcs(funcs).Parse(string(data)); err != nil {
			return nil, fmt.Errorf("cannot parse template %q: %w", name, err)
		}
		for pname, ptmpl := range ptmpls {
			tmpl.AddParseTree(pname, ptmpl.Tree)
		}
		render.Add(filepath.Base(name), tmpl)
	}

	asset_files, err := w.Horis.ReadDir("assets")
	if err != nil {
		return nil, err
	}
	for _, name := range asset_files {
		name := name
		w.Assets[filepath.Base(name)] = name
		g.GET(filepath.Base(name), func(c *gin.Context) {
			c.File(name)
		})
	}
	return render, nil
}

func (w *Web) Init(ctx context.Context) error {
	if nil == w.Horis {
		return fmt.Errorf("no associated Horis instance")
	}

	w.Assets = make(map[string]string)

	gin.SetMode(gin.ReleaseMode)
	w.engine = gin.New()
	w.engine.Use(gin.LoggerWithConfig(gin.LoggerConfig{
		SkipPaths: []string{"/ping"},
	}))
	w.engine.Use(gin.Recovery())

	csrf_buf := make([]byte, 16)
	_, err := rand.Read(csrf_buf)
	if err != nil {
		return fmt.Errorf("cannot read random bytes: %w", err)
	}
	w.token = base64.RawURLEncoding.EncodeToString(csrf_buf)

	if err := w.initRoutes(&w.engine.RouterGroup); err != nil {
		return fmt.Errorf("cannot init routes: %w", err)
	}

	render, err := w.initRender(&w.engine.RouterGroup)
	if err != nil {
		return fmt.Errorf("cannot init templates: %w", err)
	} else {
		w.engine.HTMLRender = render
	}

	w.listener, err = net.Listen("tcp", ":8775")
	if err != nil {
		return fmt.Errorf("cannot listen: %w", err)
	}

	return nil
}

func (w *Web) Run(ctx context.Context) func() error {
	w.ctx = ctx

	go func(ctx context.Context) {
		<-ctx.Done()
		w.listener.Close()
	}(ctx)

	go func() {
		if os.Getenv("NO_BROWSER") != "" {
			return
		}

		switch runtime.GOOS {
		case "linux":
			go exec.Command("xdg-open", "http://localhost:8775/admin").Run()
		case "windows":
			go exec.Command(
				"rundll32",
				"url.dll,FileProtocolHandler",
				"http://localhost:8775/admin",
			).Run()
		case "darwin":
			go exec.Command("open", "http://localhost:8775/admin").Run()
		}
	}()

	return func() error {
		return w.engine.RunListener(w.listener)
	}
}

// vim: ai:ts=8:sw=8:noet:syntax=go
