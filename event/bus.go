/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package event

import (
	"sync"
)

type domain[T interface{}] struct {
	mu   sync.Mutex
	cv   *sync.Cond
	last *T
}

func dom[T any]() *domain[T] {
	d := &domain[T]{}
	d.cv = sync.NewCond(&d.mu)
	return d
}

type Message struct {
	From string
	Body string
}

type Bus struct {
	message *domain[Message]
}

func fire[T any](d *domain[T], m *T) {
	d.mu.Lock()
	d.last = m
	d.mu.Unlock()

	d.cv.Broadcast()
}

func loop[T any](d *domain[T], done <-chan struct{}) <-chan T {
	ch := make(chan T)
	go func(ch chan T) {
		defer close(ch)

		for {
			d.mu.Lock()
			l := d.last
			for d.last == l {
				d.cv.Wait()
			}
			l = d.last
			d.mu.Unlock()

			select {
			case <-done:
				return
			case ch <- *l:
			}
		}
	}(ch)

	return ch
}

func (b *Bus) FireOnMessage(m Message) {
	fire(b.message, &m)
}

func (b *Bus) OnMessage(cb func(m Message) bool) {
	done := make(chan struct{})
	go func() {
		for m := range loop(b.message, done) {
			if !cb(m) {
				close(done)
			}
		}
	}()
}

func NewBus() *Bus {
	return &Bus{
		message: dom[Message](),
	}
}

// vim: ai:ts=8:sw=8:noet:syntax=go
