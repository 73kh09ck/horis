/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package zombie

import (
	"archive/zip"
	"io"
	"log"
	"os"
	"path/filepath"
)

func MakePK3() (*os.File, error) {
	z, err := os.CreateTemp("", "horis.*.pk3")
	if err != nil {
		return nil, err
	}

	defer func() {
		z.Close()

		if err != nil {
			os.Remove(z.Name())
		}
	}()

	w := zip.NewWriter(z)

	lump, err := w.Create("LOADACS")
	if err != nil {
		return nil, err
	}
	lump.Write([]byte("crab\n"))

	lump, err = w.Create("CVARINFO")
	if err != nil {
		return nil, err
	}
	lump.Write([]byte(`user noarchive string crabmsg = "Hello";`))
	lump.Write([]byte(`user noarchive string crabfont = "BIGFONT";`))
	lump.Write([]byte("user noarchive float crabx = 3.0;\n"))
	lump.Write([]byte("user noarchive float craby = 0.2;\n"))
	lump.Write([]byte("user noarchive float craba = 1.0;\n"))

	lump, err = w.Create("acs/")
	if err != nil {
		return nil, err
	}

	lump, err = w.Create("acs/crab.o")
	if err != nil {
		return nil, err
	}

	f, err := os.Open(filepath.Join("assets", "crab.o"))
	if err != nil {
		log.Printf("falling back on embedded crab code: %s", err)
		// TODO: implement acs assembler
		lump.Write([]byte("ACS\x00"))
		lump.Write([]byte{
			0xc4, 0x00, 0x00, 0x00,
			0x55, 0xa7, 0x00, 0xe1,
			0x57, 0xa7, 0x00, 0xc4,
			0x9d, 0xa7, 0x01, 0xe1,
			0x57, 0xa7, 0x00, 0xc5,
			0x9d, 0xa7, 0x02, 0xe1,
			0x57, 0xa7, 0x00, 0xc6,
			0x9d, 0xa7, 0x03, 0xe1,
			0x57, 0xb0, 0x00, 0x00,
			0xf0, 0x06, 0x58, 0xa7,
			0x04, 0xe1, 0x57, 0xf0,
			0x1e, 0x01, 0x00, 0x00,
		})
		lump.Write([]byte("SPTR"))
		lump.Write([]byte{
			0x08, 0x00, 0x00, 0x00,
			0xff, 0xff, 0x00, 0x00,
		})
		lump.Write([]byte{0x08, 0x00, 0x00, 0x00})
		lump.Write([]byte("SNAM"))
		lump.Write([]byte{
			0x10, 0x00, 0x00, 0x00,
			0x01, 0x00, 0x00, 0x00,
		})
		lump.Write([]byte{0x08, 0x00, 0x00, 0x00})
		lump.Write([]byte("crab"))
		lump.Write([]byte{0x00, 0x00, 0x00, 0x00})
		lump.Write([]byte("STRL"))
		lump.Write([]byte{
			0x50, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x05, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x20, 0x00, 0x00, 0x00,
			0x2d, 0x00, 0x00, 0x00,
			0x36, 0x00, 0x00, 0x00,
			0x3f, 0x00, 0x00, 0x00,
			0x4d, 0x00, 0x00, 0x00,
		})
		lump.Write([]byte(`crab:{\"x\":`))
		lump.Write([]byte{0x00})
		lump.Write([]byte(`, \"y\":`))
		lump.Write([]byte{0x00})
		lump.Write([]byte(`, \"z\":`))
		lump.Write([]byte{0x00})
		lump.Write([]byte(`, \"health\":`))
		lump.Write([]byte{0x00})
		lump.Write([]byte(`}`))
		lump.Write([]byte{0x00, 0x00})

		lump.Write([]byte("ALIB"))
		lump.Write([]byte{
			0x00, 0x00, 0x00, 0x00,
			0x34, 0x00, 0x00, 0x00,
		})
		lump.Write([]byte("ACSe"))
		lump.Write([]byte{
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
		})
	} else {
		io.Copy(lump, f)
		f.Close()
	}
	if err := w.Close(); err != nil {
		return nil, err
	}

	return z, nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
