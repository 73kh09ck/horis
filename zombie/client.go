/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package zombie

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

var ErrNotConnected = fmt.Errorf("not connected")

type State struct {
	MapLump, MapName string
	X, Y, Z          float64
	Health           int

	LastMessage string
}

type Client struct {
	target *net.UDPAddr
	c      *net.UDPConn
	ctx    context.Context
	cancel context.CancelFunc
	config *ClientConfig

	mu  *sync.Mutex
	ok  bool
	cv  *sync.Cond
	err error

	state State
}

type ClientConfig struct {
	Password string
	Timeout  time.Duration
}

func Dial(network, addr string, config *ClientConfig) (*Client, error) {
	if network != "udp" {
		return nil, fmt.Errorf("unexpected protocol: %q", network)
	}

	h, p, err := net.SplitHostPort(addr)
	if err != nil {
		return nil, err
	}

	var zone string
	if idx := strings.IndexRune(h, '%'); idx != -1 {
		zone = h[idx+1:]
		h = h[:idx]
	}

	ip := net.ParseIP(h)
	if ip == nil {
		return nil, fmt.Errorf("cannot parse IP: %q", h)
	}

	port, err := strconv.ParseUint(p, 10, 16)
	if err != nil {
		return nil, fmt.Errorf("bad port number: %q", p)
	}

	uaddr := &net.UDPAddr{
		IP:   ip,
		Port: int(port),
		Zone: zone,
	}

	c, err := net.Dial(network, addr)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(context.Background())

	mu := new(sync.Mutex)
	client := &Client{
		target: uaddr,
		config: config,
		c:      c.(*net.UDPConn),
		ctx:    ctx,
		cancel: cancel,
		mu:     mu,
		cv:     sync.NewCond(mu),
	}
	go client.connect()
	return client, nil
}

func (c *Client) connect() {
	t := time.NewTicker(time.Second)
	defer t.Stop()

	for {
		select {
		case <-c.ctx.Done():
			return
		case <-t.C:
			_, err := c.c.Write([]byte{0xff, 52, 4})
			if err != nil {
				log.Printf("write error: %s", err)
				continue
			}

			if c.config.Timeout != 0 {
				c.c.SetReadDeadline(time.Now().Add(c.config.Timeout))
			}
			buf := make([]byte, 64)
			n, err := c.c.Read(buf)
			if err != nil {
				log.Printf("read error: %s", err)
				continue
			}

			buf = buf[:n]
			if len(buf) < 2 {
				log.Printf("unexpected EOF: %x", buf)
				continue
			}

			if buf[0] != 0xff && buf[1] != 35 {
				log.Printf("unexpected reply from headcrab: %x", buf)
				continue
			}

			go c.run()
			return
		}
	}
}

func (c *Client) Close() {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.cancel()
	c.c.Close()
}

func (c *Client) State() State {
	c.mu.Lock()
	c.mu.Unlock()

	return c.state
}

func (c *Client) Subscribe(ctx context.Context) <-chan State {
	ch := make(chan State, 1)

	c.mu.Lock()
	last := c.state
	c.mu.Unlock()
	ch <- last

	current := last

	go func() {
		defer close(ch)

		for {
			c.mu.Lock()
			for {
				select {
				case <-ctx.Done():
					c.mu.Unlock()
					return
				default:
					current = c.state
				}

				if current != last {
					break
				}
				c.cv.Wait()
			}
			c.mu.Unlock()

			select {
			case <-ctx.Done():
				return
			case ch <- current:
				last = current
			}
		}
	}()

	return ch
}

func (c *Client) Send(command string) error {
	c.mu.Lock()
	defer c.mu.Unlock()

	if !c.ok {
		return ErrNotConnected
	}

	_, err := c.c.Write(append(
		[]byte{0xff, 54},
		[]byte(command)...,
	))
	return err
}

func (c *Client) process(pkt []byte) {
	if len(pkt) < 2 {
		return // unexpected EOF
	}

	if pkt[0] != 0xff {
		return // unexpected reply from headcrab
	}

	payload := pkt[2:]

	c.mu.Lock()
	defer c.mu.Unlock()

	switch pkt[1] {
	case 37:
		if strings.HasPrefix(string(payload), "crab:") {
			err := json.Unmarshal(payload[5:], &c.state)
			if err != nil {
				log.Printf("cannot unmarshal crab: %q: %s", payload, err)
			}
		} else {
			c.state.LastMessage = string(payload)
		}
	case 38:
		if len(pkt) < 3 {
			return // unexpected EOF
		}

		switch pkt[2] {
		case 2:
			payload = pkt[3:]

			parts := strings.SplitN(string(payload), " - ", 2)

			c.state.MapLump = parts[0]
			if len(parts) == 2 {
				c.state.MapName = parts[1]
			}
		}
	}

	c.cv.Broadcast()
}

func (c *Client) IsReady() bool {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.ok
}

func (c *Client) run() {
	c.mu.Lock()
	c.ok = true
	c.mu.Unlock()

	ctx := c.ctx
	buf := make([]byte, 4096)

	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		n, err := c.c.Read(buf)
		if err != nil {
			c.mu.Lock()
			defer c.mu.Unlock()
			c.err = err
			return
		}

		c.process(buf[0:n])
	}
}

// vim: ai:ts=8:sw=8:noet:syntax=go
