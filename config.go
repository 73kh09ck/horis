/**
 * Copyright 2022 go775
 *
 * Licensed under the GNU Affero General Public License (AGPL).
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package horis

import (
	"encoding/json"
	"errors"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
)

type Config struct {
	h     *Horis
	ZAddr string
	ZProg string
	ZArgs []string

	script string
}

func (c *Config) ResetToDefault() {
	c.ZAddr = "127.0.0.1:10666"
	for _, name := range []string{
		"zandronum.exe",
		"zandronum",
		"zdoom.exe",
		"zdoom",
		"gzdoom.exe",
		"gzdoom",
	} {
		resolved, err := exec.LookPath(name)
		if err != nil {
			continue
		}

		c.ZProg = resolved
		if filepath.Ext(c.ZProg) != ".exe" {
			target, err := os.Readlink(c.ZProg)
			if err != nil {
				break
			}

			if filepath.IsAbs(target) {
				c.ZProg = target
				break
			}

			target = filepath.Join(
				filepath.Dir(resolved),
				target,
			)

			c.ZProg = filepath.Clean(target)
		}
		break
	}
	c.ZArgs = []string{
		"+sv_cheats", "1",
		"+skill", "4",
	}

	script, _ := os.ReadFile(filepath.Join("assets", "default_script.ugo"))
	c.script = string(script)
}

func (c *Config) Script() string {
	return c.script
}

func (c *Config) Store() error {
	err := os.WriteFile(
		filepath.Join(c.h.horisConfigDir, "script.ugo"),
		[]byte(c.script),
		0666,
	)
	if err != nil {
		return err
	}

	f, err := os.OpenFile(
		filepath.Join(c.h.horisConfigDir, "config.json"),
		os.O_WRONLY|os.O_CREATE,
		0666,
	)
	if err != nil {
		return err
	}

	defer f.Close()
	enc := json.NewEncoder(f)
	enc.SetIndent("", "    ")
	err = enc.Encode(c)
	if err != nil {
		return err
	}

	n, err := f.Seek(0, os.SEEK_CUR)
	if err != nil {
		return err
	}

	err = f.Truncate(n)
	if err != nil {
		return err
	}

	return nil
}

func (c *Config) Load() error {
	f, err := os.Open(filepath.Join(c.h.horisConfigDir, "config.json"))
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			c.ResetToDefault()
			err = c.Store()
		}
		return err
	}

	defer f.Close()
	dec := json.NewDecoder(f)

	err = dec.Decode(c)
	if err != nil {
		return err
	}

	script, err := os.ReadFile(filepath.Join(c.h.horisConfigDir, "script.ugo"))
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			script, _ = os.ReadFile(filepath.Join("assets", "default_script.ugo"))
			os.WriteFile(filepath.Join(c.h.horisConfigDir, "script.ugo"), script, 0666)
		} else {
			return err
		}
	}

	c.script = string(script)

	return nil
}

// vim: ai:ts=8:sw=8:noet:syntax=go
